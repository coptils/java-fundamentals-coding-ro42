package com.sda;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Choose from menu below");
        System.out.println("1.Calculate perimeter");
        System.out.println("2.Calculate BMI");

        int menuOption = scanner.nextInt();
        switch (menuOption) {
            case 1:
                calculatePerimeter();
                break;
            case 2:
                calculateBMI();
                break;
            default:
                System.out.println("Invalid option");
        }

        String str1;
        String str2 = "second string";
        String str3, str4 = "fourth string", str5;
        str5 = "second string";
        str1 = "first string";
        str3 = new String("second string");
        str3 = "third value";

        final String CONST_TEST;
        CONST_TEST = "should be final";
//        CONST_TEST = "test";
        final String CONST_TEST2 = "value";
//        CONST_TEST2 = "djdnw";

        System.out.println("Finish!");
    }

    /**
     * Write an application that will read diameter of a circle (variable of type float) and
     * calculate perimeter of given circle.
     * Firstly, assume π = 3.14. Later, use value of π from built-in Math class.
     */
    private static void calculatePerimeter() {
//        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the diameter of the circle:");
        float diameter = scanner.nextFloat();
//        float perimeter =(float)(diameter*Math.PI);
        double perimeter = diameter * Math.PI;
        System.out.printf("The perimeter of the circle with diameter=%.2f is: %.2f", diameter, perimeter);
    }

    /*       Write an application calculating BMI (Body Mass Index) and checking if itЀs optimal or not.
          Your application should read two variables: weight (in kilograms, type float) and height
      (in centimeters, type int). BMI should be calculated given following formula:
          The optimal BMI range is from 18.5 to 24.9, smaller or larger values are non-optimal
          values. Your program should write "BMI optimal" or "BMI not optimal", according to the
          assumptions above.*/
    private static void calculateBMI() {
        float weight;
        System.out.println("Insert weight(in kg):");
//        Scanner scanner = new Scanner(System.in);
        weight = scanner.nextFloat();
        int height;
        System.out.println("Insert height(in cm):");
        height = scanner.nextInt();
        float heightInMeters = height / 100.0f;
        float BMI = weight / (heightInMeters * heightInMeters);
        if (BMI > 18.5 && BMI < 24.9) {
            System.out.println("BMI optimal");
        } else {
            System.out.println("BMI not optimal");
        }
    }
}
