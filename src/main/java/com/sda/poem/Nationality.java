package com.sda.poem;

public enum Nationality {
    ROMANIAN, POLISH, IRISH, ITALIAN, ENGLISH
}
