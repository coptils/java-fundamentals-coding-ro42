package com.sda.poem;

public class Poem {
    private int stropheNumbers;
    private Author creator;

    public int getStropheNumbers() {
        return stropheNumbers;
    }

    public Author getCreator() {
        return creator;
    }

    public void setStropheNumbers(int stropheNumbers) {
        this.stropheNumbers = stropheNumbers;
    }

    public void setCreator(Author creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        return "Poem{" +
                "stropheNumbers=" + stropheNumbers +
                ", creator=" + creator +
                '}';
    }
}
