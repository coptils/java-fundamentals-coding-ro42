package com.sda.poem;

import java.time.LocalDate;

/**
 * Write an application that consists of few classes:
 * a. Author class, representing an author – poem writer, which consists of fields surname and nationality (both of type String)
 * b. Poem class, representing poem, which consists of fields creator (type Author) and stropheNumbers (type int – numbers of strophes in poem)
 * c. Main class, with main method, inside which you will:
 * i. Create three instances of Poem class, fill them with data (using constructor and/or setters) and store them in array
 * ii. Write a surname of an author, that wrote a longest poem (let your application calculate it!)
 */
public class RequirementV2 {
    public static void main(String[] args) {
        Author author1 = new Author("Eminescu", "roman", LocalDate.of(1850, 01, 15));
        Author author2 = new Author("Alecsandri", "roman", LocalDate.of(1821, 07, 21));

        AuthorV2 autor3 = new AuthorV2("Ion", Nationality.ENGLISH, LocalDate.of(1989,12,25));
        AuthorV2 autor4 = new AuthorV2("Andrei",Nationality.ROMANIAN,LocalDate.of(1992,07,9));
        System.out.println(autor3);
        System.out.println(autor4);

        Poem poem1 =new Poem();
        poem1.setStropheNumbers(100);
        poem1.setCreator(author1);
        Poem poem2 = new Poem();
        poem2.setStropheNumbers(400);
        poem2.setCreator(author2);
        Poem poem3 = new Poem();
        poem3.setStropheNumbers(300);
        poem3.setCreator(author1);

        Poem poems[] = new Poem[]{poem1, poem2, poem3};
        Poem poemWithMaxStrophe = getPoemWithMaxStrophe(poems);
        System.out.println("The creator of the poem with maxim of strophes is:" + poemWithMaxStrophe.getCreator().getSurname());

        Poem poems1[] = new Poem[]{poem1, poem2};
        Poem poemWithMaxStrophe1 = getPoemWithMaxStrophe(poems1);
        System.out.println("The creator of the poem with maxim of strophes is:" + poemWithMaxStrophe1.getCreator().getSurname());

    }
    private static Poem getPoemWithMaxStrophe(Poem[] poems) {
        Poem poemWithMaxStrophe = poems[0];
        for (Poem i : poems) {
            System.out.println(i);
            if (i.getStropheNumbers() > poemWithMaxStrophe.getStropheNumbers()) {
                poemWithMaxStrophe = i;
            }

        }
        return poemWithMaxStrophe;
    }
}
