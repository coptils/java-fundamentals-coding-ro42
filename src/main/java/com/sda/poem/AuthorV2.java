package com.sda.poem;

import java.time.LocalDate;

public class AuthorV2 {
   private String surname;
   private Nationality nationality;
   private LocalDate dateOfBirth;

   public AuthorV2(String surname, Nationality nationality, LocalDate dateOfBirth ){
      this.surname=surname;
      this.nationality=nationality;
      this.dateOfBirth=dateOfBirth;
   }

   public String getSurname() {
      return surname;
   }

   @Override
   public String toString() {
      return "Author{" +
              "surname='" + surname + '\'' +
              ", nationality='" + nationality + '\'' +
              ", dateOfBirth=" + dateOfBirth +
              '}';
   }
   /* public Author() {
   }*/
}



