package com.sda.poem;

import java.time.LocalDate;

public class Author {
   private String surname;
   private String nationality;
   private LocalDate dateOfBirth;

   public Author (String surname,String nationality, LocalDate dateOfBirth ){
      this.surname=surname;
      this.nationality=nationality;
      this.dateOfBirth=dateOfBirth;
   }

   public String getSurname() {
      return surname;
   }

   @Override
   public String toString() {
      return "Author{" +
              "surname='" + surname + '\'' +
              ", nationality='" + nationality + '\'' +
              ", dateOfBirth=" + dateOfBirth +
              '}';
   }
   /* public Author() {
   }*/
}



