package com.sda.petrol;

import java.util.Random;

public class Pump {
    private double totalAmountOfFuel;

    public Pump() {
        this.totalAmountOfFuel = 0.0;
    }

    public Pump pumpFuel() {
        this.totalAmountOfFuel += new Random().nextDouble(1.0, 10.0);
        return this;
    }

    public double getTotalAmountOfFuel() {
        return totalAmountOfFuel;
    }

}