package com.sda.petrol;

public class Driver {

    private String name;
    private double balance;

    public Driver(String name){
      this.name= name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double pay(double finalPrice){
        return  finalPrice-balance;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                '}';
    }
}

