package com.sda.petrol;

import java.util.Scanner;

public class PetrolStation {
       private double fuelPrice;
       private String name;

       public PetrolStation(double fuelPrice,String name){
           this.fuelPrice=fuelPrice;
           this.name=name;
       }

       public double calculateFinalPrice(Pump pump) {

           return fuelPrice * pump.getTotalAmountOfFuel();
       }

        /** The user paid exactly as much as required.
        * The user paid too much (cashier should return the rest of the money).
        * The user paid too little – should be asked for the rest.
        */
       public void processPayment(Driver driver, Pump pump){
             double priceToPay=calculateFinalPrice(pump);
           System.out.printf("Dear %s, you have to pay total price of: %f RON.%n", driver.getName(), priceToPay);
           while(true) {
               System.out.println("Please enter your amount you want to pay:");
               Scanner scanner = new Scanner(System.in);
               double providedAmount = scanner.nextDouble();
               driver.setBalance(providedAmount);
               double result = driver.pay(priceToPay);
               if (result == 0) {
                   System.out.println("Thank you! See you next time!");
                   break;
               } else if (result > 0) {
                   priceToPay = result;
                   System.out.printf("You still have to pay %f RON %n", priceToPay);
               } else {
                   System.out.printf("Here is your change, %f RON", Math.abs(result));
                   break;
               }
           }
       }

}
