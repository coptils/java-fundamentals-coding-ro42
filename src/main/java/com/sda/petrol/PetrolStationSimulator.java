package com.sda.petrol;

import java.util.Scanner;

public class PetrolStationSimulator {
    public void processSimulation(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hello Driver, please enter your name: ");
        Driver driver1 = new Driver(scanner.nextLine());
        PetrolStation omvStation = new PetrolStation(9.15, "omv");
        Pump pump1 = new Pump();
        while (true){
            System.out.printf("%s, would you like to continue , or would you like to finish?%n", driver1.getName());
            String option = scanner.nextLine();
            if(option.equals("finish")){
                omvStation.processPayment(driver1, pump1);
                break;
            }
            if(option.equals("continue")) {
                System.out.printf("You have added %f liters of fuel into your car%n", pump1.pumpFuel().getTotalAmountOfFuel());
                System.out.printf("You have to pay %f RON%n", omvStation.calculateFinalPrice(pump1));
            }
        }
    }

}
