package com.sda.circle;

/**
 * Writes a class named Circle to represent a circle that has two properties:
 * radius (of type double) and color (of type String).
 * The class properties should not be accessible from outside the class (class encapsulation).
 * <p>
 * The radius and color should have default values, 1.0 and "red", respectively.
 * The Circle class should have two overloaded constructors.
 * One constructor with no arguments (which sets both radius and color to default).
 * And one constructor with given radius, but color default.
 * The Circle class should have two public method:
 * first method will allow us to retrieve radius;
 * and the second method will allow us to retrieve area of the circle;
 * 1. Modify the class Circle to include a third constructor for constructing a Circle instance with the given radius and color.
 * 2. Modify the test program TestCircle to construct an instance of Circle using this constructor.
 * 3. Add a getter for variable color for retrieving the color of a Circle instance.
 * 4. Modify the test program to test this method.
 * 5. Is there a need to change the values of radius and color of a Circle instance after it is constructed? If so, add two public methods called setters for changing the radius and color of a Circle instance.
 * 6. Modify the TestCircle to test these methods
 * 7. Implement a toString() method and test it;
 */
public class Requirement {
    public static void main(String[] args) {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle();
        boolean compareResult = circle1.equals(circle2);
        boolean compareResult1 = circle1.equals(123);
        boolean compareResult2 = circle1.equals("str2");
        System.out.println(circle1.toString()); // apel explicit al metodei toString() -> redundant (se face oricum acest apel)
        System.out.println(circle2.toString());
        System.out.println(compareResult);
        System.out.println(compareResult1);
        System.out.println(compareResult2);
        System.out.println(circle1 == circle2);

        Circle circle3= new Circle(4);
        System.out.println(circle3);  // apel implicit al metodei toString
        double a = circle3.getRadius();
        System.out.println(a);
        double ariaOfCircle3= circle3.computeArea();
        System.out.printf("Aria cercului 3 este: %.2f %n finish %n", ariaOfCircle3);
        Circle circle4= new Circle(7,"blue");
        System.out.println(circle4);

        System.out.printf("Aria cercului 4 este: %.2f %n finish %n", circle4.computeArea());
        circle4.setColor("white");
        System.out.println(circle4);

    }
}
