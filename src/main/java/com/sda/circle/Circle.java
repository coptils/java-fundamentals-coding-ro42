package com.sda.circle;

import java.util.Objects;

public class Circle {
    private double radius;
    private String color;

    public Circle() {
        radius = 1.0;
        color = "red";
    }
    // constructor overloading -> avem un parametru in plus fata de constructorul anterior
    public Circle(double radius){
        this(); // apel la constructorul default declarat anterior; !! pe prima linie din constructor
        this.radius=radius; // cu this. accesam fieldurile si metodele clasei
    }

    // constructor overloading; se schimba lista de parametrii
    public Circle(double radius, String color){
        this(radius); // apel la constructorul anterior
        this.color=color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getRadius() {
        return radius;

    }

    public double computeArea(){
        return radius*radius*Math.PI;
    }


    /*
    @Override
    public boolean equals(Object circleToCompareWith) {
        if (this.getClass() != circleToCompareWith.getClass()) {
           return false;
        }
        Circle castedCircle = (Circle) circleToCompareWith;
        if (this.radius == castedCircle.radius && this.color.equals(castedCircle.color)) {
            return true;
        }
        return false;
    }
*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0 && Objects.equals(color, circle.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(radius, color);
    }

    @Override
    public String toString() {
        String displayMessage = "radius = " + radius + "; " + "color = " + color;
        return displayMessage;
    }


}
