package com.sda.agenda;

/**
 * Agenda telefonica
 * Creati o agenda pentru numere de telefon de capacitate 100;
 * Putem sa adaugam un numar in agenda doar daca nu s-a atins capacitatea maxima.
 * Putem sa cautam in agenda dupa un numar de telefon sa vedem daca acesta exista sau nu.
 * Putem sa afisam toate numerele de telefon existente in agenda.
 */
public class Requirement {
    public static void main(String[] args) {

        Contact contact1 = new Contact(); // obiect de tipul Contact creat prin folosirea constructorului default
        // setam valori pentru obiectul contact1 ==> alternativa de a seta valori field-urilor disponibile pe un obiect este prin apelarea Constructorului care ne permite sa dam valori field-urilor
        contact1.setName("Tudor");
        contact1.setPhoneNumber("0749089249");
        contact1.setAge(45);
        // System.out.println(contact1.toString());

        Contact contact2 = new Contact("Elena", "0745123456", 110); // am creat un obiect si am setat si valori in acelasi timp
        //   System.out.println(contact2);

        Phonebook phonebook1 = new Phonebook(); // un obiect de tipul Phonebook
        phonebook1.addContact(contact1);
        phonebook1.addContact(contact2);
        phonebook1.addContact(new Contact("Mihai", "072334455", 56));
        phonebook1.addContact(new Contact("Andrei", "072355455", 58));
        phonebook1.addContact(new Contact("Ovidiu", "072366455", 8));
        System.out.println("=================");

        System.out.println(phonebook1); // se apeleaza implicit metoda toString()
        boolean result = phonebook1.searchContact(contact2); // metoda searchContact returneaza un rezultat de tipul boolean
        System.out.println(result);
        System.out.println(phonebook1.searchContact(new Contact("Andrei", "072355455", 58)));
        System.out.println(phonebook1.searchContact(new Contact("Mihai", "072334455", 56)));
        System.out.println(phonebook1.searchContact(new Contact("Alexandru", "072399455", 56)));
        System.out.println("===================");
        System.out.println(phonebook1.searchPhoneNumber("0749089249"));
        System.out.println(phonebook1.searchPhoneNumber("0125089249"));
    }
}
