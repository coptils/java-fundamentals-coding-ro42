package com.sda.agenda;

import java.util.Arrays;

public class Phonebook {
    private Contact[] contacts; // un tablou de obiecte de tipul Contact
    private int counter = 0;

    public Phonebook() {
        contacts = new Contact[100];
    }

    public boolean addContact(Contact contactToBeAdded) {
        if (counter < 100) {
            this.contacts[counter] = contactToBeAdded;
            counter = counter + 1;
            return true;
        } else {
            System.out.println("Phonebook is full");
            return false;
        }
    }

    public boolean searchContact(Contact contactToSearchFor) {
        int index = 0;
        for (Contact contactFromCurrentIndex : contacts) {        //enhanced for --> itereaza prin tot array-ul (pana la ultimul index)
            // contactFromCurrentIndex se schimba la fiecare iteratie si are tipul Contact deoarece iteram peste un array de Contact (Contact[] contacts)
            // iteratia 1: contactFromCurrentIndex = contacts[0];
            // iteratia 2: contactFromCurrentIndex = contacts[1];
            // ........
            // iteratia length: contactFromCurrentIndex = contacts[length-1];
            //
            if (index == counter) {
                break;
            }
            index++;
            if (contactFromCurrentIndex.equals(contactToSearchFor)) {
                return true;
            }
        }
        return false;
    }

    public boolean searchPhoneNumber(String phoneNumberToSearchFor) {
        for (int iii = 0; iii < counter; iii++) {
            if (contacts[iii].getPhoneNumber().equals(phoneNumberToSearchFor)) {
                return true; // intoarce un rezultat -> se iese din metoda daca conditia este indeplinita
            }
        }
        return false; // intoarce false ca si rezultat
    }

    @Override
    public String toString() {
        String result = "";
        for (int indexCounter = 0; indexCounter < counter; indexCounter++) {
            result += indexCounter + " -> ";
            result += contacts[indexCounter];
            result += "\n";
        }
        return result;
    }
}
