package com.sda.agenda;

import java.util.Objects;

public class Contact {
    private String name;
    private String phoneNumber;
    private int age;

    public Contact(String name, String phoneNumber, int age) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.age = age;
    }

    // suprascrierea constructorului default oferit de clasa parinte Object
    // acest constructor default se pierde odata ce am adaugat un constructor intr-o clasa, iar daca avem nevoie de el,
    // trebuie sa il adaugam explicit
    public Contact (){
    }

    // getter folosit pentru a obtine valoarea field-ului name din afara clasei
    public String getName() {
        return name;
    }

    // cu setters putem sa dam valori field-urilor (alternativa = constructori)
    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // metoda suprascrisa din clasa parinte Object (am modificat implementarea ei)
    // adnotarea e pur informativa
    @Override
    public String toString() {
        // tipul returnat dintr-o metoda trebuie sa corespunda cu tipul din definitia metodei
        return "Contact{" +
                "name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", age=" + age +
                '}';
    }

    // equals si hashCode trebuie generate impreuna (Alt+Insert -> Equals&HashCode)
    // prin equals comparam obiecte de orice tip (!inclusiv string-uri)
    // prin "==" comparam referinte de memorie
    @Override
    public boolean equals(Object o) {
        if (this == o) return true; // daca obiectul care se compara cu obiectul de comparat au aceleasi referinte in metorie -> obiectul identitate (se compara un obiect cu el insusi) -> equals returneaza true
        if (o == null || getClass() != o.getClass()) return false; // daca obiectul de comparat este null -> obiectul care se compara nu are cum sa fie egal cu null (!!!NullPointerExceptions -> nu ajungeam pana aici)  SAU cele doua obiecte care se compara nu au acelasi tip ==> equals returneaza false
        // daca am trecut de validarea anterioara => obiectul cu care se compara nu este null si ambele obiecte sunt de acelasi tip
        Contact contact = (Contact) o;  // facem casting = dintr-un tip mai mare (Object) punem intr-un tip mai mic (Contact) -> de ce? ca sa avem acces la field-urile si metodele din clasa Contact (!!!clasa Object nu are field-urile: name, phoneNumber, age)
        return Objects.equals(phoneNumber, contact.phoneNumber); // Objects.equals compara doua string uri intre ele
    }

    // hasCode -ul trebuie generat deodata cu equals si metoda Objects.hash(field1, field2....) -> !!! trebuie sa contina doar field-urile care se compara in equals
    @Override
    public int hashCode() {
        return Objects.hash(phoneNumber);
    }
}


