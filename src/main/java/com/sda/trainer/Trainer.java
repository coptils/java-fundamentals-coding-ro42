package com.sda.trainer;

public class Trainer {

    private String name;
    private Trainee trainee;

    public Trainer(String name, Trainee trainee) {
        this.name = name;
        this.trainee = trainee;
    }

    public String getName() {
        return name;
    }

    public Trainee getTrainee() {
        return trainee;
    }
}
