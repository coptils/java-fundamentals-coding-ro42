package com.sda.trainer;

public class Trainee {

    private String name;
    private int strength;

    public Trainee(String name) {
        this.name = name;
        this.strength = 100;
    }

    public String getName() {
        return name;
    }

    public int getStrength() {
        return strength;
    }

    public Trainee setStrength(int strength) {

        if (strength > 0) {

            this.strength = strength;
        } else {
            System.out.println("Please add correct strength: ");
        }
        return this;

    }

    public boolean doExercises(Exercises exercise) {
        if (this.strength > exercise.getStrength()) {
            this.setStrength(this.strength - exercise.getStrength());
            System.out.println("I did complete the exercise with name " + exercise.getName() + " of type " + exercise.getType());
        return true;
        } else {
            System.out.println("Not able to complete the exercise with name " + exercise.getName() + " of type " + exercise.getType());
        return false;
        }

    }

    public void eatSupplement(Supplements supliment) {
        this.setStrength(this.strength + supliment.getStrengthAdded());
        System.out.println("My new strength is:  "  + this.strength);
    }
}
