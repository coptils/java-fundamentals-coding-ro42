package com.sda.trainer;

public class Exercises {

    private String name;
   private  ExercisesType type;
   private int strength;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ExercisesType getType() {
        return type;
    }

    public void setType(ExercisesType type) {
        this.type = type;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }
}
