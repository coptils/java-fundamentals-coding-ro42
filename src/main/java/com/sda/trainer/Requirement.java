package com.sda.trainer;

import java.util.Random;

/**
 * Personal Trainer
 * <p>
 * Create an app that will simulate a Personal Trainer System
 * Think about the required models and create the required classes.
 * <p>
 * You’ll simulate both sides – Trainer and Trainee.
 * Within a while loop you will be asked for an exercise to be done.
 * Every exercise should add/reduce the strength of the trainee.
 * Take into account that strength should not be reduced below 0.
 * Consider adding some supplements that will recover the strength.
 */
public class Requirement {
    public static void main(String[] args) {

        Trainee trainee1 = new Trainee("Alex");
        trainee1.setStrength(500);
        Trainer trainer1 = new Trainer("Lacatus", new Trainee("Gigel").setStrength(300));
        while (true) {
            Exercises ex = new Exercises();
            ex.setType(ExercisesType.AEROBIC);
            ex.setName("ex" + new Random().nextInt(1, 1000));
            ex.setStrength(new Random().nextInt(301, 400));
            System.out.println("PLease do next exercise with name: " + ex.getName() + " of strength " + ex.getStrength()
            );
            boolean res = trainer1.getTrainee().doExercises(ex);

              if (!res)
             {
                Supplements supp = new Supplements("Supplimente" + new Random().nextInt(1, 1000), new Random().nextInt(30, 75));
                trainer1.getTrainee().eatSupplement(supp);

                 boolean res1 = trainer1.getTrainee().doExercises(ex);

                 if (!res1){
                     break;

                 }

            }


        }


    }

    private static boolean hasStrength(Trainer trainer1, Exercises ex) {
        return trainer1.getTrainee().getStrength() > ex.getStrength();
    }
}