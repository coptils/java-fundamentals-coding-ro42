package com.sda.trainer;

public class Supplements {

    private String name;
    private int strengthAdded;

    public Supplements(String name, int strengthAdded) {
        this.name = name;
        this.strengthAdded = strengthAdded;
    }

    public String getName() {
        return name;
    }

    public int getStrengthAdded() {
        return strengthAdded;
    }
}
