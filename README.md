# java-coding-fundamentals

# Git intro
- Open source => free
- Tool => ne permite sa urmarim schimbarile dintr-un fisier (VCS)
- Folosit pentru a coordona munca programatorilor in timpul dezvoltarii software
- Cel mai folosit VCS din lume => considerat standardul modern pentru dezvoltarea software
- Distribuit => fiecare developer poate sa aiba local un istoric full al codului sursa dintr-un repository aflat pe un server
- Alte tool-uri asemanatoare, dar centralizate: SVN, CVS
- Ofera suport pentru:
    - Branching
    - Merging
    - Pull Requests + Code Review
    - Repository History

# Git workflow
- Crearea unui repository (proiect), folosind un git hosting tool: Bitbucket, Gitlab, Github
- Copierea (clonarea) repository-ului pe masina locala a dezvolatorului
- Crearea unui branch de lucru din cel principal (master/main)
- Adaugarea de modificari sau fisiere noi in repo-ul local (copiem un proiect java pe care vrem sa il urcam pe git)
- Commit-ul (salvarea modificarilor)
- Push-ul schimbarilor din repo-ul local pe server-ul gazda
- Deschiderea unui Pull Request
- Asteptarea dupa Review-uri din partea colaboratorilor de pe proiect
- Merge-ul branch-ului de lucru inapoi in branch-ul principal (master/main)

# Git cheatsheet
Pasii necesari pentru a crea un nou proiect (repository) in Gitlab:
1. Din Gitlab (https://gitlab.com/), apasam butonul **New project** asa cum arata in imaginea de mai jos:
   ![img.png](img.png)
2. La pasul urmator, alegem obtiunea **Create blank project**
3. Da-i un nume proiectului, de exemplu: my-first-project, configureaza-l ca in imaginea de mai jos si apasa butonul **Create project**
   ![img_1.png](img_1.png)

Pasii necesari pentru a clona/copia local proiectul creat anterior in Gitlab:
1. Mai intai copiaza in clipboard url-ul proiectului din Gitlab: Clone -> Clone with HTTPs, ca in imaginea urmatoare:
   ![img_2.png](img_2.png)
2. Deschide un terminal local (Gitbash) si navigheaza spre directorul unde vrei sa clonezi proiectul
    - pentru a crea un director nou local, folosim comanda:
        - ***mkdir nume_director***
    -  pentru a naviga in directul nou creat, folosim comanda:
        - ***cd nume_director***
3. Din directorul unde vrei sa clonezi proiectul, ruleaza urmatoarea comanda:
    - ***git clone linkul_din_pasul1***
    - ex. ***git clone https://gitlab.com/coptils/my-first-project.git***
4. Navigheaza in proiectul tocmai clonat local, folosind **cd my-first-project**, de unde poti sa executi urmatoarele comenzi de Git:
    - pentru vizualizare status al branch-ului curent:
        - ***git status***
    - pentru a adauga schimbarile locale in staging (le pregatim pentru a le comite)
        - ***git add .*** => adauga toate fisierele modificate in staging
        - ***git add filename*** => adauga doar fisierul mentionat in staging
    - pentru a salva modificarile in staging (a comite):
        - ***git commit -m "Un mesaj care sa descrie modificarile facute"***
    - pentru a publica remote (pe server) modificarile locale:
        - ***git push***
        - ***git push origin nume_branch***  (daca suntem pe un branch local)
    - pentru a new actualiza/sincroniza cu remote repository (repository ul de pe server):
        - ***git pull***
        - ***git pull origin nume_branch*** (daca suntem pe un branch local)
    - crearea unui nou branch:
        - ***git checkout -b nume_branch***
    - schimbare branch:
        - ***git checkout nume_branch***
        - ex: ***git checkout main*** => ne duce de pe branchul curent pe branchul main
    - ca sa aducem schimbarile dintr-un branch(de ex. test_branch1) in branchul curent:
        - ***git merge test_branch1***  => resolve conflicts if they exists
        - ***git merge --continue*** => after resolving conflicts (only in case of conflicts)
        - ***git status*** (to see if there are more local changes)
        - ***git add .*** (if there are more local changes)
        - ***git commit -m "descritive message"*** (if there were more local changes)
        - ***git push origin nume_branch*** (to push the changes after merge on your branch)
    - stergere branch local:
        - ***git branch -d local_branch_name***

# Comenzi de baza Git
- https://learngitbranching.js.org/

# Comenzi de baza Linux care pot fi rulate din Gitbash
- pentru a naviga in directul nou creat:
    - ***cd nume_director***
- pentru a crea un director nou local:
    - ***mkdir nume_director***
- pentru a sterge un director creat:
    - ***rm -fr nume_director***
- pentru a crea un fisier text:
    - ***touch textfile_name.txt***
- pentru a vedea continutul unui fisier:
    - ***cat textfile_name.txt***
- pentru a deschide un fisier text folosind vim:
    - ***vim textfile_name.txt***
    - pentru a scrie in fisier, apasam tasta "i" si putem incepe sa scrie
    - pentru a iesi din editorul de text vim fara a salva modificarile facute, folosim combinatia de taste: "ESC" + ":q!"
    - pentru a iesi din editorul de text vim si a salva modificarile facute, folosim combinatia de taste: "ESC" + ":wq!"